// eslint-disable-next-line
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Loading = () => (
  <div>
    Loading <FontAwesomeIcon icon="sync" />
  </div>
);

export default Loading;
