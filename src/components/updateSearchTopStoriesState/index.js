// eslint-disable-next-line
import React from 'react';
import PropTypes from 'prop-types';

const updateSearchTopStoriesState = (hits, page) => prevState => {
  const { searchKey, results } = prevState;
  const oldHits = results && results[searchKey] ? results[searchKey].hits : [];
  const updatedHits = [...oldHits, ...hits];

  return {
    results: {
      ...results,
      [searchKey]: { hits: updatedHits, page }
    },
    isLoading: false
  };
};

updateSearchTopStoriesState.propTypes = {
  hits: PropTypes.object,
  page: PropTypes.number,
  prevState: PropTypes.object
};

export default updateSearchTopStoriesState;
