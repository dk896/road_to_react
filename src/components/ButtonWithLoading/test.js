import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import ButtonWithLoading from './';

describe('ButtonWithLoading', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
      <ButtonWithLoading isLoading={true} onClick={() => ''}>
        Press Me
      </ButtonWithLoading>,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  });

  test('has a valid snapshort isLoading=true', () => {
    const component = renderer.create(
      <ButtonWithLoading isLoading={true} onClick={() => ''}>
        More
      </ButtonWithLoading>
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('has a valid snapshort isLoading=false', () => {
    const component = renderer.create(
      <ButtonWithLoading isLoading={false} onClick={() => ''}>
        More
      </ButtonWithLoading>
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
