// eslint-disable-next-line
import React from 'react';
import Button from '../Button';
import withLoading from '../withLoading';

const ButtonWithLoading = withLoading(Button);

export default ButtonWithLoading;
