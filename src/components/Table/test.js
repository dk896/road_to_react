// eslint-disable-next-line
import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Table from './';

Enzyme.configure({ adapter: new Adapter() });

describe('Table', () => {
  const props = {
    list: [
      {
        title: 'Super theme',
        author: 'John',
        num_comments: 10,
        points: 13,
        objectID: '234'
      },
      {
        title: 'Redux not dad',
        author: 'Georg',
        num_comments: 15,
        points: 19,
        objectID: '235'
      }
    ],
    onDismiss: () => '',
    onSort: () => '',
    sortKey: 'NONE',
    isSortReverse: false
  };

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Table {...props} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('has a valid snapshot', () => {
    const component = renderer.create(<Table {...props} />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('shows two items in list', () => {
    const element = shallow(<Table {...props} />);

    expect(element.find('.table-row').length).toBe(2);
  });
});
